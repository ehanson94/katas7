const arr = ['item1', 'item2', 'item3', 'item4', 'item5']
const arr2 = [1, 2, 3, 4, 5]
const arr3 = [12, 3, 30, 6, 20]

//new forEach function
function newForEach(array, functionToDo) {
    for (let index = 0; index < array.length; index += 1) {
        functionToDo(array[index], index, array)
    }
}

function printItems(item, index) {
    const output = `This is ${item} and it is index number ${index + 1}`
    console.log(output)
    return output
}

newForEach(arr, printItems)

//new map function
function newMap(array, functionToDo) {
    const result = []
    
    for (let index = 0; index < array.length; index += 1) {
        result[index] = functionToDo(array[index], index, array)
    }
    console.log(result)
    return result
}

function arrayPow(num) {
    const result = Math.pow(num, 2)
    return result
}

newMap(arr2, arrayPow)

//new some function
function newSome(array, functionToDo) {
    for (let index = 0; index < array.length; index += 1) {
        if (functionToDo(array[index], index, array)) {
            console.log(true)
            return true
        }
    }

    console.log(false)
    return false
}

function findEvens(num) {
    return (num % 2 === 0)
}

newSome(arr2, findEvens)

//new find function
function newFind(array, functionToDo) {
    for (let index = 0; index < array.length; index += 1) {
        if (functionToDo(array[index], index, array)) {
            console.log(array[index])
            return array[index] 
        }
    }

    console.log(undefined)
    return undefined
}

function findNumberGreater(num) {
    return (num > 15)
}

newFind(arr3, findNumberGreater)

//new findIndex function
function newFindIndex(array, functionToDo) {
    for (let index = 0; index < array.length; index += 1) {
        if (functionToDo(array[index], index, array)) {
            console.log(index)
            return index
        }
    }

    console.log(-1)
    return -1
}

newFindIndex(arr3, findNumberGreater)

//new every function
function newEvery(array, functionToDo) {
    for (let index = 0; index < array.length; index += 1) {
        if (!functionToDo(array[index], index, array)) {
            console.log(false)
            return false
        }
    }
    console.log(true)
    return true
}

function isBelowNumber(num) {
    return (num < 35)
}

newEvery(arr3, isBelowNumber)

//new filter function
function newFilter(array, functionToDo) {
    const results = []
    for (let index = 0; index < array.length; index += 1) {
        if (functionToDo(array[index], index, array) !== undefined) { 
        results.push(functionToDo(array[index], index, array))
        }
    }
    console.log(results)
    return results
}

function newArray(num) {
    if (num < 20) {
        return num
    }
}

newFilter(arr3, newArray)